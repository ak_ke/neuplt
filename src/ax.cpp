
#include "ax.h"

int Ax::draw() {
    glColor3f(0,0,0);
    glBegin(GL_LINES);
        glVertex2f(x0, y0);
        glVertex2f(xN, y0);

        glVertex2f(xN, y0);
        glVertex2f(xN, yN);

        glVertex2f(xN, yN);
        glVertex2f(x0, yN);

        glVertex2f(x0, yN);
        glVertex2f(x0, y0);
    glEnd();

    for(int i=0; i<pltN; i++)
        plts[i].draw();

    return 0;
}

Ax::Ax(float x0, float y0, float xN, float yN) {
    this->x0 = x0;
    this->y0 = y0;
    this->xN = xN;
    this->yN = yN;

    pltN = 0;
    plts = NULL; 
};

Ax::~Ax() {
    if(plts) {
        //for(int i=0; i<pltN; i++)
        //    delete plts[i];
        delete[] plts;
    }
}
