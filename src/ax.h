#ifndef AX_H
#define AX_H

#include <GLFW/glfw3.h>

#include "plt.h"

class Plt;

class Ax {
public:
    float x0, y0, xN, yN;
    int pltN;
    Plt* plts;

    Ax(float x0, float y0, float xN, float yN);
    ~Ax();
    int draw();
    int addPlt(pltT type, const char* fn, const char* args...);
};

#endif
