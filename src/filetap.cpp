#include "filetap.h"

void Filetap::init(std::string fn, Plt* plt) {
    this->fn = fn;
    ifs.open(fn);
    this->plt = plt;
}

int Filetap::poll() {
    if ( !ifs.is_open() )
        return -1;
    while(!ifs.eof())
        while(std::getline(ifs, line)) {
            parse_line(line);
            plt->m = (plt->m+1) %  plt->M;
        }
    ifs.clear();

    return 0;
}

int Filetap::parse_line(std::string line) {
    n = 0;
    switch(plt->N) {
        case -1:
        {   // get N from line and allocate plt->dat
            std::string cline = line;
            plt->N = 0;
            cline = line;
            while ( !cline.empty() ) {
                plt->N++;
                if ( cline.find(",") == std::string::npos )
                    break;
                cline = cline.substr(cline.find(",")+1);
            }
            for(unsigned int m=0; m<plt->M; m++)
                plt->dat[m] = new double[plt->N];
        }
        default:
            while ( !line.empty() ) { 
                plt->dat[plt->m][n] = std::atof(line.substr(0, line.find(",")).c_str());
                if ( line.find(",") == std::string::npos)
                    break;
                line = line.substr(line.find(",")+1);
                if ( n >= plt->N )
                    break; 
            }
            break;
    }
    return 0;
}
