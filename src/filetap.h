#ifndef FILETAP_H
#define FILETAP_H

#include <fstream>
#include <string.h>

#include "plt.h"

class Plt;

class Filetap {
public:
    std::string fn;
    std::ifstream ifs;
    Plt* plt;   // pointer to plt parent

    std::string line;
    unsigned int interval;  // polling interval

    void init(std::string fn, Plt* plt);
    int parse_line(std::string line);
    int poll();

    unsigned int n;
};

#endif
