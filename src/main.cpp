#include <iostream>
#include <unistd.h>
#include <vector>

#include <GLFW/glfw3.h>

#include "win.h"

// forward declarations:
void error_callback(int err, const char* descr);
int initGLFW();

int main(int argc, char* argv[]) {

    // TODO: add arg parsing ---------------------------------------- |
    float Dt = 0.1; // seconds
    unsigned int M = 512;   // length of t range to plot
    unsigned int Ndat = 512*4;  // length of dat
    unsigned int Nwin = 1;
    //               |t |
    //               |  |(t-Dt*Ndat)
    // [Ndat0, 1, 2, 3, 4, 5, 6, 7 ]
    //        [       ] // animate plotting
    //                  // when end is reached, start
    //                  // again with end of plotted interval
    //                  // == last written by filetap
    //                  // This needs some tuning for refresh
    //                  // rate
    // -------------------------------------------------------------- |

    initGLFW();
    // load in things -------------
    // // load wins, ax, plts, files
    // ............................
    std::vector<Win*> wins;
    for(int i=0; i<Nwin; i++) {
        wins.push_back(new Win(10, 10, 1, 1));
    }
    std::vector<Win*>::iterator winit;

    // add axes:
    wins[0]->addAx(0, 0, 1, 1);

    // loop
    float t = 0;
    while(wins.size() > 0) {
        winit = wins.begin();
        while(winit != wins.end()) {
            (*winit)->update(t);
            if( !(*winit)->draw() ) {
                // delete window:
                delete (*winit);
                (*winit) = NULL;
                winit = wins.erase(winit);
            } else
                winit++;
        }

        t += Dt;
        usleep(Dt*10e5);
    }

    // clean up:
    for(winit = wins.begin(); winit != wins.end(); winit = wins.erase(winit)); {
        delete (*winit);
        (*winit) = NULL;
    };
    glfwTerminate();

    return 0;
}

int initGLFW() {
    if(!glfwInit()) {
        fprintf(stderr, "Error in GLFW init\n");
    }

    glfwSetErrorCallback(error_callback);

    return 1;
}

void error_callback(int err, const char* descr) {
    fprintf(stderr, "Error: %s (%d)\n", descr, err);
}
