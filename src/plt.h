/* ----------------------------------------------------------------- *|
    class Plt. Takes care of the plotting of lines, points, etc.
        This class is virtual. Real plotting needs to be done by
        deriving classes

    Part of datoscope (git@gitlab.com:ak_ke/datoscope.git)

        (Akke Mats Houben; akkehouben@gmail.com; akkeh.github.io)
|* ----------------------------------------------------------------- */

#ifndef PLT_H
#define PLT_H

#include "filetap.h"
//#include "ax.h"

enum pltT {
    xy
};

class Filetap;
class Ax;

class Plt {
public:
    Filetap* filetap;
//    Ax* ax; // axes to draw on

    unsigned int M = 512;   // length of buffer (time)
    int N = -1;             // # of columns (variables)
    double** dat;           // the data
    unsigned int m = 0;     // write pointer (time)

    int update();
    int draw();

    Plt(std::string fn, unsigned int M);
    ~Plt();

    float max_v, min_v;
    float lastmax_v, lastmin_v;
};

#endif
