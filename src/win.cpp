#include "win.h"

#include <iostream>

int Win::addAx(int r0, int c0, int rN, int cN) {
    float x0 = -1+marg_w + col_w*(float)c0+(c0>0)*col_sep/2;
    float y0 = -1+marg_h + row_h*(float)r0+(r0>0)*row_sep/2;
    float xN = x0 + col_w*((float)(cN-c0))-((float)(cN-c0)<Ncols)*col_sep/2;
    float yN = y0 + row_h*((float)(rN-r0))-((float)(rN-r0)<Nrows)*row_sep/2;

    Ax** tax = this->axs;
    this->axs = new Ax*[Nax+1];
    for(int i=0; i<Nax; i++)
        this->axs[i] = tax[i];
    this->axs[Nax++] = new Ax(x0, y0, xN, yN);
    delete[] tax;
    return 0;
}

Win::Win(int w, int h, int Nrows, int Ncols) {
    this->w = w; this->h = h;
    this->Nrows = Nrows; this->Ncols = Ncols;
    
    marg_w = 0.1; marg_h = 0.1;
    col_w = (2-2*marg_w) / ((float)Ncols);
    row_h = (2-2*marg_h) / ((float)Nrows);
    row_sep = 0.1*(Nrows>1);
    col_sep = 0.1*(Ncols>1);

    r = 0; c = 0;
    Nax = 0;
    axs = NULL;
    win = glfwCreateWindow(w, h, "neuPlt", NULL, NULL);
    if(!win)
        printf("Error in window creation!\n");

    // GLFW init:
    glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);    // win resize
    glfwSetKeyCallback(win, key_callback);
    glfwMakeContextCurrent(win);
    glfwSwapInterval(1);
    // ...
    glfwSetWindowShouldClose(win, GLFW_FALSE);
    glClearColor(1,1,1,1);
};

void Win::key_callback(GLFWwindow* win, int key, int scancode, int action, int mods) {
    if(action == GLFW_TRUE) {
        switch(key) {
            case GLFW_KEY_ESCAPE:
                glfwSetWindowShouldClose(win, GLFW_TRUE);
            break;

            default:
                ;
            break;
        }
    }
}

bool Win::update(unsigned long t) {
    // update axes
    return 0;
}

bool Win::draw() {
    glfwMakeContextCurrent(win);
    glfwSwapBuffers(win);
    glfwPollEvents();

    glClear(GL_COLOR_BUFFER_BIT);
    for(int i=0; i<Nax; i++)
        axs[i]->draw();

    glfwGetFramebufferSize(win, &w, &h);
    glViewport(0,0,w,h);

    return !glfwWindowShouldClose(win);
}

Win::~Win() {
    if(this->axs) {
        for(int i=0; i<this->Nax; i++)
            delete axs[i];
        delete[] axs;
    };

    glfwDestroyWindow(this->win);
};
