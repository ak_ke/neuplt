#ifndef WIN_H
#define WIN_H

#include <GLFW/glfw3.h>

#include "ax.h"
/*          w
    |---------------------------------------|
    |  ]     marg_h                      [  |
  h |=======================================|
    |  ](r0,c0)  | |(r0,c1)  | |(r0,c2)  [  |
    |  ]         |c|         |c|         [  |
    | m]         |o|         |o|         [m |
    | a]         |l|         |l|         [a |
    | r]---------|_|---------|_|---------[r |
    | g] row_sep |s| row_sep |s| row_sep [g |
    | _]---------|e|---------|e|---------[_ |
    | w](r1,c0)  |p|(r1,c1)  |s|(r1,c2)  [w |
    |  ]        c| |         | |         [  |
    |  ]        o| |         | |         [  |
    |  ]        l| |         | |         [  |
    |  ]        _| |         | |         [  |
    |  ]col_w   h| |         | |         [  |
    |=======================================|
    |  ]     marg_h                      [  |
    |---------------------------------------|
*/
class Win {
public:
    int w, h;               // width, height
    int Nrows, Ncols;         // # of rows, cols
    float col_w, row_h;     // col width, row height
    int r, c;               // currently filled
    float marg_w, marg_h;   // margin w, margin h
    float col_sep, row_sep; // col, row separation
    GLFWwindow* win;

    int Nax;    // # of axes
    Ax** axs;   // axes

    Win(int w, int h, int Nrows, int Ncols);
    static void key_callback(GLFWwindow* win, int key, int scancode, int action, int mods);
    bool update(unsigned long t);
    bool draw();
    int addAx(int r0, int c0, int R, int C);
    ~Win();
};

#endif
